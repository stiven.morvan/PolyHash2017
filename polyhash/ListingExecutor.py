from polyhash.EffectiveCell import EffectiveCell


class ListingExecutor:

    def __init__(self, map):
        self.map = map
        self.listing_grid = self.generate_listing_grid()
        self.listing_cells = self.generate_list_of_coverable_cells()

    def generate_listing_grid(self):
        listing_grid = []
        for y in range(self.map.rows):
            listing_grid.append([])
            for x in range(self.map.columns):
                listing_grid[y].append(EffectiveCell(self.map.get_value(x, y), listing_grid))
        return listing_grid

    def generate_list_of_coverable_cells(self):

        coverable_cells = []
        for y in range(len(self.listing_grid)):
            for x in range(len(self.listing_grid[y])):
                listing_cell = self.listing_grid[y][x]
                if listing_cell.cell.value == ".":
                    coverable_cells.append(listing_cell)
                    listing_cell.is_on_border = listing_cell.check_if_on_border()
        for coverable_cell in coverable_cells:
            coverable_cell.calc_borders()
        return coverable_cells

    def get_listing_cell(self, cell):
        return self.listing_grid[cell.y][cell.x]

    def run(self, distance_size_pivot, ratio_taille_max):
        choosen_listing_cells = []

        self.listing_cells.sort()
        taille_max = len(self.listing_cells[0].effective_covered_cells)
        print(taille_max)

        while True:

            self.listing_cells.sort()
            listing_cell_pivot = self.listing_cells[0]

            if len(listing_cell_pivot.effective_covered_cells) == 0:
                # S'il n'y a plus de cases a couvrir on arrete
                routers = []
                choosen_listing_cells.sort()
                for listing_cell in choosen_listing_cells:
                    routers.append(listing_cell.cell)
                return routers, self.listing_grid

            for listing_cell in self.listing_cells:
                if len(listing_cell.effective_covered_cells) != 0 and (len(listing_cell.effective_covered_cells) == len(listing_cell_pivot.effective_covered_cells) or
                                                                           (len(listing_cell.effective_covered_cells) >= len(listing_cell_pivot.effective_covered_cells) - distance_size_pivot
                                                                            and  len(listing_cell.effective_covered_cells) > taille_max * ratio_taille_max)):
                    if len(listing_cell.effective_covered_cells_on_borders) > len(listing_cell_pivot.effective_covered_cells_on_borders):
                        listing_cell_pivot = listing_cell
                else:
                    break

            cell_pivot = listing_cell_pivot.cell

            print(cell_pivot, " # "+str(len(listing_cell_pivot.effective_covered_cells)))

            for temp in listing_cell_pivot.effective_covered_cells:
                listing_cell_temp = self.get_listing_cell(temp)
                if listing_cell_temp.is_on_border:
                    for parent in listing_cell_temp.effective_covered_cells:
                        listing_cell_parent = self.get_listing_cell(parent)
                        if temp in listing_cell_parent.effective_covered_cells_on_borders:
                            listing_cell_parent.effective_covered_cells_on_borders.remove(temp)

                for y in range(temp.y - 1, temp.y + 2):
                    for x in range(temp.x - 1, temp.x + 2):
                        if self.listing_grid[y][x].cell not in listing_cell_pivot.effective_covered_cells:
                            self.listing_grid[y][x].is_on_border = True
                            for cell in self.listing_grid[y][x].effective_covered_cells:
                                listing_cell = self.get_listing_cell(cell)
                                if self.listing_grid[y][x].cell not in listing_cell.effective_covered_cells_on_borders:
                                    listing_cell.effective_covered_cells_on_borders.append(self.listing_grid[y][x].cell)

            for temp in cell_pivot.covered_cells:
                listing_cell_temp = self.get_listing_cell(temp)
                for parent in temp.covered_cells:
                    listing_cell_parent = self.get_listing_cell(parent)
                    if temp in listing_cell_parent.effective_covered_cells:
                        listing_cell_parent.effective_covered_cells.remove(temp)
                    if parent in listing_cell_temp.effective_covered_cells:
                        listing_cell_temp.effective_covered_cells.remove(parent)

            choosen_listing_cells.append(listing_cell_pivot)
            self.listing_cells.remove(listing_cell_pivot)
            self.listing_cells.sort()
