class Cell:

    def __init__(self, x, y, value):
        self.x = x
        self.y = y
        self.value = value
        self.covered_cells = []

    def __lt__(self, other):
        nb_self = len(self.covered_cells)
        nb_other = len(other.covered_cells)

        return nb_self > nb_other

    def __hash__(self):
        return hash((self.x, self.y))

    def __eq__(self, other):
        return other and self.x == other.x and self.y == other.y

    def __str__(self):
        return str(self.y)+' '+str(self.x)
