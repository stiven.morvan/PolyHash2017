from tkinter import *
from polyhash.Map import Map


class MapDisplay(Tk):
    uncovered_cell = "red"
    covered_cell = "green"
    void_color = "white"
    wall_color = "black"
    backbone_color = "yellow"
    router_color = "blue"
    initial_backbone_color = "orange"

    def __init__(self, input_file_path, output_file_path):
        super().__init__()
        self.map = Map(input_file_path)
        self.backbones, self.routers = self.read_output_file(output_file_path)
        self.canvas = Canvas(self, width=self.map.columns, height=self.map.rows)
        self.canvas.pack()

        self.draw_map()
        self.draw_covered_cells()
        self.draw_backbones()
        self.draw_routers()
        self.draw_initial_backbone()

        mainloop()

    def draw_point(self, x, y, color):
        self.canvas.create_rectangle(x, y, x, y, fill=color, width=0)

    def draw_map(self):
        if self.map:
            for y in range(self.map.rows):
                for x in range(self.map.columns):
                    color = {
                        ".": MapDisplay.uncovered_cell,
                        "#": MapDisplay.wall_color,
                        "-": MapDisplay.void_color
                    }.get(self.map.get_value(x, y).value)
                    self.draw_point(x, y, color)

    def draw_initial_backbone(self):
        initial_backbone_cell = self.map.initial_backbone_cell
        self.draw_point(initial_backbone_cell.x, initial_backbone_cell.y, MapDisplay.initial_backbone_color)

    def read_output_file(self, output_file_path):
        file = open(output_file_path, 'r')

        index = 0
        backbones = []
        routers = []

        for line in file:
            values = line.split(" ")
            if len(values) == 1:
                index += 1
            else:
                x = int(values[1])
                y = int(values[0])
                cell = self.map.get_value(x, y)

                if index == 1:
                    backbones.append(cell)
                if index == 2:
                    routers.append(cell)

        return backbones, routers

    def draw_covered_cells(self):
        for router in self.routers:
            router.covered_cells = self.map.covered_point(router.x, router.y)
            self.draw_cells(router.covered_cells, MapDisplay.covered_cell)

    def draw_backbones(self):
        self.draw_cells(self.backbones, MapDisplay.backbone_color)

    def draw_routers(self):
        self.draw_cells(self.routers, MapDisplay.router_color)

    def draw_cells(self, cells, color):
        for cell in cells:
            self.draw_point(cell.x, cell.y, color)
