from polyhash.Snake import *
import bresenham


class BackboneExecutor:

    def __init__(self, map, routers):
        for y in range(map.rows):
            for x in range(map.columns):
                map.get_value(x, y).value = False
        map.initial_backbone_cell.value = True
        self.map = map
        self.not_connected_routers = list(routers)
        self.snakes = [Snake(map.initial_backbone_cell, self)]
        self.backbones = []
        self.iteration = 0

    def add_found_cell(self, snake, found_cell):
        found_cell.value = True
        if found_cell in self.not_connected_routers:
            self.add_found_router(snake, found_cell)

    def add_found_router(self, snake, router):
        # Generation des backbones pour connecter le nouveau routeur
        self.generate_backbones_with_closest(router)
        # Retirer le routeur de la liste des routeurs non découverts/connectés
        self.not_connected_routers.remove(router)
        # Création nouveau snake correspondant au routeur trouvé
        new_snake = Snake(router, self)
        self.snakes.append(new_snake)
        snake.children.append(new_snake)

    def generate_backbones_with_closest(self, router):
        closest_backbone = self.map.initial_backbone_cell
        min_dist = self.distance(closest_backbone, router)
        for backbone in self.backbones:
            # Trouver le backbone le plus proche auquel se raccorder
            dist = self.distance(backbone, router)
            if dist <= min_dist:
                min_dist = dist
                closest_backbone = backbone
        self.generate_backbones(closest_backbone, router)

    def generate_backbones(self, start, end):
        points = list(bresenham.bresenham(start.x, start.y, end.x, end.y))
        del points[0]

        for point in points:
            self.backbones.append(self.map.get_value(point[0], point[1]))

    def distance(self, cell1, cell2):
        x1 = cell1.x
        y1 = cell1.y
        x2 = cell2.x
        y2 = cell2.y
        diff_x = abs(x1 - x2)
        diff_y = abs(y1 - y2)
        distance = abs(diff_x - diff_y) + min(diff_x, diff_y) - 1
        return distance

    def run(self):
        while len(self.not_connected_routers) > 0:
            for snake in self.snakes:
                snake.children.clear()
                snake.move_forward(self.iteration)
            self.iteration += 1
        return self.backbones
