def write_file(nom_fichier, backbones, routers):

    fichier = open(nom_fichier, "w")
    fichier.write(str(len(backbones)) + "\n")
    for backbone in backbones:
        fichier.write(str(backbone) + "\n")

    fichier.write(str(len(routers)) + "\n")

    for r in routers:
        fichier.write(str(r.y) + " " + str(r.x) + "\n")

    fichier.close()
