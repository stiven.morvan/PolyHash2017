from polyhash.BackboneExecutor import BackboneExecutor


def optimizer(liste, map):
    """
    Utilisation du buget restant apres listing

    :param liste: on recommence a partir de la ou on s'est arrete
    :param map:
    :return: les nouveaux routeurs et backbones
    """
    routers = liste[0]
    routers_effective_grid = liste[1]
    backbones = []

    nunbers_routeurs = 0
    budget_remaining = 0
    current_budget = 0

    while True:
        print("budget_actuel "+str(current_budget))
        print("Backbones : "+str(len(backbones)))
        print("budget_restant : "+str(budget_remaining))
        print("Routeurs : "+str(nunbers_routeurs))
        print("nombre_routeurs : "+str(nunbers_routeurs))

        if nunbers_routeurs > len(routers):
            nunbers_routeurs = len(routers)

        if current_budget > map.maximum_budget:
            temp = routers_effective_grid[routers[nunbers_routeurs - 1].y][routers[nunbers_routeurs - 1].x]

            while len(temp.effective_covered_cells) != 0:
                enfant = temp.effective_covered_cells[0]
                i = nunbers_routeurs
                found = False
                while i < len(routers) and not found:
                    if enfant in routers[i].covered_cells:
                        found = True
                        routers_effective_grid[routers[i].y][routers[i].x].effective_covered_cells.append(enfant)
                    i += 1

                del temp.effective_covered_cells[0]
            next = list(routers[nunbers_routeurs:])
            suite_effective = []
            for i in range(len(next)):
                suite_effective.append(routers_effective_grid[next[i].y][next[i].x])

            suite_effective.sort()
            next = []

            for c in suite_effective:
                next.append(c.cell)
            del routers[nunbers_routeurs-1:]
            routers.extend(next)
            nunbers_routeurs -= 1

        be = BackboneExecutor(map, routers[0:nunbers_routeurs])
        backbones = be.run()

        current_budget = nunbers_routeurs * map.router_price + len(backbones) * map.backbone_price
        budget_remaining = map.maximum_budget - current_budget

        if budget_remaining >= map.router_price + map.backbone_price and nunbers_routeurs < len(routers):
            nunbers_routeurs += map.estimate(budget_remaining)
            if map.estimate(budget_remaining) == 0:
                nunbers_routeurs += 1
        elif budget_remaining >= 0:
            print("STOP")
            print("budget_actuel "+str(current_budget))
            print("Backbones : "+str(len(backbones)))
            print("budget_restant : "+str(budget_remaining))
            print("Routeurs : "+str(nunbers_routeurs))
            print("nombre_routeurs : "+str(nunbers_routeurs))
            return routers[0:nunbers_routeurs], backbones
