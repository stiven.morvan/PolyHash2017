class Snake:

    def __init__(self, cell, executor):
        self.cell = cell
        self.executor = executor
        self.head_x = cell.x
        self.head_y = cell.y - 1
        self.nbr_tour = 1
        self.nbr_iteration = 0
        self.direction = 0  # 0 = gauche; 1 = haut; 2 = droite; 3 = bas
        self.discovered_a_cell_during_tour = False
        self.children = []

    def move_forward(self, iteration):
        # Faire avancer le snake tant qu'il n'a pas parcourue la longueur donnée en paramètre
        while self.nbr_iteration < iteration:
            self.nbr_iteration += 1

            # Faire en sorte que les snakes "enfants" avancent en même temps sans dépasser leur "parent"
            for child in self.children:
                child.move_forward(self.nbr_iteration)

            head_cell = self.executor.map.get_value(self.head_x, self.head_y)
            if head_cell and not head_cell.value:
                # Cellule non découverte auparavant
                self.discovered_a_cell_during_tour = True
                # Ajout de la cellule trouvée pour tester si il s'agit d'un routeur à connecter
                self.executor.add_found_cell(self, head_cell)
            if self.direction == 0:
                if self.cell.x - self.head_x == self.nbr_tour:
                    self.direction = 1
                self.head_x -= 1

            elif self.direction == 1:
                if self.cell.y - self.head_y == self.nbr_tour:
                    self.direction = 2
                self.head_y -= 1

            elif self.direction == 2:
                if self.head_x - self.cell.x == self.nbr_tour:
                    self.direction = 3
                self.head_x += 1

            elif self.direction == 3:
                if self.head_y - self.cell.y == self.nbr_tour:
                    self.nbr_tour += 1
                    self.direction = 0
                    # Si le snake n'a pas pu découvrir de nouvelles cellules en 1 tour, il doit s'arrêter
                    if not self.discovered_a_cell_during_tour and self.nbr_iteration == iteration:
                        self.executor.snakes.remove(self)
                        for snake in self.executor.snakes:
                            if self in snake.children:
                                snake.children.remove(self)
                                snake.children.extend(self.children)
                                break
                        return
                    self.discovered_a_cell_during_tour = False
                self.head_y += 1
