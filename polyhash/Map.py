from polyhash.Cell import Cell


class Map:

    def __init__(self, chemin, rows=None, columns=None, grid=None, router_price=None, backbone_price=None, router_coverage=None,
                 maximum_budget=None, initial_backbone_cell=None, estimate_nbr_routeur=None):
        self.rows = rows
        self.columns = columns
        self.grid = grid
        self.router_price = router_price
        self.backbone_price = backbone_price
        self.router_coverage = router_coverage
        self.maximum_budget = maximum_budget
        self.initial_backbone_cell = initial_backbone_cell
        self.estimate_nbr_routeur = estimate_nbr_routeur
        self._read_map(chemin)

    def get_value(self, x, y):
        """

        :param x:
        :param y:
        :return: La valeur visé dans la grille (None si x et y pas correcte)
        """
        if 0 <= y < self.rows:
            if 0 <= x < self.columns:
                return self.grid[y][x]
        return None

    def covered_map(self):
        """
        Initialise les cases a porte de la case teste et ceux pour toute les cases de la liste
        :return:
        """
        for y in range(len(self.grid)):
            for x in range(len(self.grid[y])):
                if self.get_value(x, y) is not None and self.get_value(x, y).value == ".":
                    temp = self.grid[y][x]
                    self.grid[y][x].covered_cells.append(temp)
                    self.grid[y][x].covered_cells.extend(self.covered_point(x,y))

    def covered_point(self, x, y):
        """
        Parcourt iteratif autour du point donner en parametre
        :param x:
        :param y:
        :return:
        """
        liste = []
        dist = self.router_coverage

        signes = [1, -1]
        x_min = x - dist
        x_max = x + dist
        y_min = y - dist
        y_max = y + dist
        for signe in signes:
            arret = False
            y_temp = y + signe
            while arret is False and ((signe == 1 and y_temp <= y + dist) or (signe == -1 and y - dist <= y_temp)):
                if self.get_value(x, y_temp).value == ".":
                    liste.append(self.grid[y_temp][x])
                else:
                    if signe == -1:
                        y_min = y_temp - signe
                    else:
                        y_max = y_temp - signe
                    arret = True
                y_temp += signe

        for signe in signes:
            arret = False
            x_temp = x + signe
            while arret is False and ((x - dist <= x_temp and signe == -1) or (x_temp <= x + dist and signe == 1)):
                if self.get_value(x_temp, y).value == ".":
                    liste.append(self.grid[y][x_temp])
                else:
                    if signe == -1:
                        x_min = x_temp - signe
                    else:
                        x_max = x_temp - signe
                    arret = True
                x_temp += signe

        liste.extend(self.covered_diagonal(-1, -1, x - 1, y - 1, x_min, y_min))
        liste.extend(self.covered_diagonal(1, -1, x + 1, y - 1, x_max, y_min))
        liste.extend(self.covered_diagonal(-1, 1, x - 1, y + 1, x_min, y_max))
        liste.extend(self.covered_diagonal(1, 1, x + 1, y + 1, x_max, y_max))

        return liste

    def covered_diagonal(self, x_signe, y_signe, x, y, x_max, y_max):
        liste = []
        if self.get_value(x, y).value == "." and ((x_signe == 1 and x <= x_max) or (x_signe == -1 and x >= x_max)) and ((y_signe == 1 and y <= y_max) or (y_signe == -1 and y >= y_max)):
            liste.append(self.grid[y][x])

            # On parcour la ligne correspondante
            stop = False
            temp_x = x + x_signe
            while stop != True and (x_signe == 1 and temp_x <= x_max) or (x_signe == -1 and temp_x >= x_max):
                if self.get_value(temp_x, y).value == ".":
                    liste.append(self.grid[y][temp_x])
                else:
                    x_max = temp_x - x_signe
                    stop = True
                temp_x += x_signe

            # On parcour la colonne correspondante
            stop = False
            temp_y = y + y_signe
            while stop != True and (y_signe == 1 and temp_y <= y_max) or (y_signe == -1 and temp_y >= y_max):
                if self.get_value(x, temp_y).value == ".":
                    liste.append(self.grid[temp_y][x])
                else:
                    stop = True
                    y_max = temp_y - y_signe
                temp_y += y_signe

            x += x_signe
            y += y_signe

            liste.extend(self.covered_diagonal(x_signe, y_signe, x, y, x_max, y_max))

        return liste

    def estimate(self, budget=None):
        """
        Estimation du nombre de routeurs par rapport au budget initial
        """
        if budget is None:
            budget = self.maximum_budget
        return budget // (self.router_price + (2*self.router_coverage*self.backbone_price))

    def _read_map(self, fichier):
        """
        :param fichier: Fichier .in du sujet
        :return: type map avec ses variables instancies.
        Contenu liste variables:
        ligne (0), colonne (1), radius router (2)
        backbone cost (3) , router costs (4) , budget (5)
        coordonne initial (6) (7)

        _read_map('chemin/vers/le/fichier.in')
        grid[colonne][ligne]
        """

        fichier = open(fichier, 'r')
        i = 0
        x = 0
        listeVariables = []
        grid = []

        for ligne in fichier:

            #Recuperation des variables
            if 0 <= i and i <= 2:
                ligne = ligne.split(' ')

                for nbr in ligne:
                    listeVariables.append(int(nbr))

            #Recuperation de la grilles (liste de liste)
            else:
                grid.append([])
                for carac in ligne:
                    grid[i-3].append(Cell(x, (i-3), carac))
                    x += 1

            i += 1
            x = 0

        fichier.close()

        self.grid = grid
        self.rows = listeVariables[0]
        self.columns = listeVariables[1]
        self.router_coverage = listeVariables[2]
        self.backbone_price = listeVariables[3]
        self.router_price = listeVariables[4]
        self.maximum_budget = listeVariables[5]
        self.initial_backbone_cell = grid[listeVariables[6]][listeVariables[7]]
        self.estimate_nbr_routeur = self.estimate()
        self.covered_map()
