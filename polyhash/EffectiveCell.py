class EffectiveCell:

    def __init__(self, cell, grid):
        self.cell = cell
        self.effective_covered_cells = list(cell.covered_cells)
        self.effective_covered_cells_on_borders = []
        self.grid = grid
        self.is_on_border = False

    def __lt__(self, other):
        nb_self = len(self.effective_covered_cells)
        nb_other = len(other.effective_covered_cells)
        return nb_self > nb_other

    def __eq__(self, other):
        return other and self.cell.x == other.cell.x and self.cell.y == other.cell.y

    def check_if_on_border(self):
        for y in range(self.cell.y - 1, self.cell.y + 2):
            for x in range(self.cell.x - 1, self.cell.x + 2):
                if self.grid[y][x].cell.value != ".":
                    return True
        return False

    def calc_borders(self):
        for cell in self.effective_covered_cells:
            listing_cell = self.grid[cell.y][cell.x]
            if listing_cell.is_on_border and cell not in self.effective_covered_cells_on_borders:
                self.effective_covered_cells_on_borders.append(cell)
