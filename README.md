Google HashCode 2017
===================
### Présentation du projet
>Ce projet a pour but de définir et concevoir une solution au problème de la phase finale du Google HashCode de 2017. Le but est couvrir la plus grande surface possible sur plusieurs cartes à l'aide de bornes wifi.

----------

### Composition de l'équipe
L'équipe est composé de quatre membres:
> - Stiven Morvan
> - Colin Treal
> - Johan Sorette
> - Yves Le Palud

----------
### Répartition des tâches
Notre projet s'est déroulé en plusieurs grandes étapes. La première impliquant tout les membres du groupes et consistant à se retrouver tous ensemble pour réfléchir sur tableau à une solution possible aux différents problème à résoudre: Comment placer de manière optimales les bornes, comment les relier efficacement..
Par la suite, différentes étapes ont été réalisés, il a en effet fallu commencé à  développer notre solution, chacun ayant réalisé des parties de code, parfois seul ou en groupe. Nous avons également continuer à réfléchir en groupe régulièrement à différentes optimisations possible ou encore au moyen de résoudre certains bugs.. La majeure partie de ce projet à donc été réalisé en groupe s'agissant d'un projet de groupe avant tout. 

----------

### Procédure d'installation

Vous souhaitez tester notre code? Rien de plus simple, vous devez simplement suivre la procédure suivante afin de récuperer nos données:

- En premier lieu, vous aurez besoin de la version 3 de python pour pouvoir exécuter notre programme
- Pour récupérer nos données, à l'aide d'un terminal linux, placez vous dans le dossier ou vous souhaitez récupérer les différents fichiers et utilisez la commande suivante :
> git clone https://gitlab.univ-nantes.fr/jsorette/polyhash.git

- Vous aurez également besoin d'un module qui n'est pas installé avec python de base, pour le récupérer, il vous suffira d’exécuter dans votre terminal linux cette ligne de commande: 
> python3 -m pip install bresenham

------------

### Procédure d’exécution
Afin d’exécuter notre programme, il vous suffit de vous rendre dans le répertoire où vous avez installé nos fichier et de lancer le fichier main.py suivi de la carte en paramètre sur laquelle vous voulez disposer les routeurs.
Le programme vous affichera tout au long de son exécution les différents points qu"il sélectionne, et à la fin vous affichera le budget restant, le nombre de routeurs placés ainsi que le nombre de backbones. 

----------

### Les stratégies mises en œuvres
 Notre stratégie s'articule en 4 étapes majeures : 

- INITIALISATION : 
>Au commencement, l’algorithme lit les données et initialise différents objets 

- COUVERTURE :
>La première grande étape est le calcul des couvertures: chaque case est analysée et contient des références vers les cases qu’elle pourrait couvrir s’il y avait un routeur à cet endroit.

- LISTING :
>Ensuite, il y a un tri de toute les cellules par le nombre de cases qu’elles peuvent couvrir. A chaque étape, on va sélectionner la première qui deviendra pivot. Toutes les cellules couvertes par le pivot seront déréférencées par toutes les autres cellules. Dans le cas où plusieurs cellules ont la même couverture, on préférera une cellule qui contient une couverture en contact avec un mur ou la couverture d’une autre cellule.

- LINKING :
>Une fois la liste des routeurs à placer obtenu, il ne reste plus qu’à les relier avec des câbles. Pour cela il y a les “spirales”, chacune d’entre elles parcourt la carte en tournant et en s’éloignant progressivement autour d’un routeur. Lorsqu’un routeur est découvert, on cherche le câble (ou le routeur) le plus proche sur la map  entière. Ensuite on génère tous les câbles pour lier les deux. Puis on créé une spirale partant de la case du routeur.

----------
### L'organisation du code

Notre code s'organise en différente classes et fichiers:

Le fichier main.py:
>Fonction principale du projet, il permet de lancer l’exécution de ce dernier.

Le fichier BackboneExecutor.py:
>Contient la classe permettant la génération du backbone afin de relier tous les routeurs de la carte entre eux, en partant du backbone initial, ainsi que des méthodes qui y sont liés.

Le fichier Cell.py:
>Contient la classe permettant de représenter chaque cellule de la carte.

Le fichier EffectiveCell.py:
> Contient la classe EffectiveCell différenciée de Cell puisque celle-ci permet de stocker une couverture effective (couverture de cellules supplémentaire qu'elle offre par rapport aux cellules effectives déjà sélectionnées), ainsi que les bordures (murs et cellules déjà couvertes) qui varient lors de l'exécution du programme. Il contient également les méthodes liés à cette classe comme la méthode permettant de déterminer toutes les cellules qui sont bordures à partir de la couverture effective

Le fichier ListingExecutor.py: 
> Contient la classe permettant de générer la liste des points les plus importants de la carte en fonction du nombres de cases à porté ainsi que des méthodes qui y sont liés comme la méthode qui sélectionne les cell en fonction de leur couverture.

Le fichier Map.py:
> Contient les données de la carte (budget, grille de la map, budget...) ainsi que diverses méthodes lié à la map comme la méthode d'estimation du nombre de routeurs par rapport au budget initial.

Le fichier Snake.py:
> Contient la classe permettant de parcourir séquentiellement les cellules autour de la cellule initiale, sous forme de spirale/serpent ainsi que la méthode de déplacement du snake/spirale.

Le fichier optimizer.py:
> Contient la fonction d'ajustement du budget pour maximiser le nombre de cellules couvertes tout en minimisant les coûts :
    - ajout de nouveaux routeurs si le budget le permet
    - suppression des routeurs les moins intéressants si le budget est dépassé

Le fichier write_file.py:
 >Contient la fonction d'écriture du fichier de sortie sous le format suivant 
>> - Nombre de backbones
>> -  Positions des backbones
>> - Nombre de routeurs
>> -  Positions des routeurs

----------

### Bugs et limites connues
Un problème de notre programme se situe sur un choix réalisé en fin de projet, ce choix s'est réalisé autour d'une question très simple : à partir de quel moment doit-on privilégier de placer un routeur le plus près des bordures/murs possibles au dépend de sa couverture? 
Afin de répondre à cette question, nous avons fait l'utilisation d'une constante choisi par le biais de différent test sur les cartes. Cette constante se situe au niveau de la condition sur le listing, on enlève cette constante à la couverture de la case qu'on étudie afin de pouvoir prendre en compte certaines cases avec certes une couverture moindre mais étant placé de manière beaucoup plus intéressante (on considére qu'un routeur avec plus de bordures est plus intéressant qu'un autre).
Une des améliorations possible de notre programme serait donc de remplacer cette constante en réfléchissant à un moyen de modéliser cette question par une fonction mathématique par exemple.

----------

### Résultats :

- Charleston road :
> - 21 962 796 points
> - 3min d'execution

- Rue de londres :
> - 58 824 044 points
> - 15min d'execution

- Opera :
> - 175 150 023 points
> - 30min d'execution

- Let's go higher :
> - 290 217 917 points
> - 45min d'execution

(execution sur un processeur intel i5-4590)

Pandiculation

