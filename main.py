#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from polyhash.ListingExecutor import ListingExecutor
from polyhash.optimizer import optimizer
from polyhash.write_file import write_file
from polyhash.Map import Map
import sys
import time

liste = ["charleston_road", "lets_go_higher", "opera", "rue_de_londres"]


distance_liste = [7,8,9,10]
ratio_taille_max = 0.7

for distance in distance_liste:

    for carte in liste:

        argument = carte

        start_time = time.time()
        print("Initialisation de la carte "+carte)
        map = Map("carte/" + argument + ".in")

        print("Listing des points important")
        routers = ListingExecutor(map).run(distance,ratio_taille_max)

        print("Optimisation du budget")
        final_routers, backbones = optimizer(routers, map)

        print("Ecriture du fichier de sortie")
        write_file("resultats/distance_" + str(distance) + "arg_" + argument + ".out", backbones, final_routers)
        end_time = time.time()

        print("Le temps d execution est de ", int(end_time - start_time), "secondes.")
